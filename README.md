# Coding test for Web Developer Position

## Setup

This is a PHP application built on a LAMP stack. I suggest using the MAMP program or similar to run a local instance of this application.

1. Import the database to your MySQL instance from the `db/db_focus_famille_2018_articles.sql` file.
2. Go to the `config.php` file and enter in your database particulars (User, host, password, database name, etc)
3. You should be able to navigate to a local version of the website (localhost:8888) and the database connection should work.
4. The `index.php` will display all of the Articles if this is working correctly. A working example of this page can be found [here](https://articles.focusfamille.ca).

## Instructions

Your instructions for the assignment are on the `index.php` page. You will be making a "Search by date" function for the page. All of the details and considerations are mentioned there.

## Submission

Clone this repo and work on it from there. When you are done, provide a link to your solution on your personal Gitlab, Github, or similar. Your work must be completed before your next interview.

At your interview, we will look discuss your solution and how you solved the problem.

## Questions

If you have trouble setting up the webapp or have questions about the requirements, contact peterd@fotf.ca.